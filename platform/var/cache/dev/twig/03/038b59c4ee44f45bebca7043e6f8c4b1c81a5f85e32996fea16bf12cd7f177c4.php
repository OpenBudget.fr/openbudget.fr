<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_4021f0c2e43eab030c9c8b819e421a45d28eb088495bf100c9699aba9922bd1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9e49bb7c7859c421919f425d3830852924b51c2465a667f1c2bfda2be29818aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e49bb7c7859c421919f425d3830852924b51c2465a667f1c2bfda2be29818aa->enter($__internal_9e49bb7c7859c421919f425d3830852924b51c2465a667f1c2bfda2be29818aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9e49bb7c7859c421919f425d3830852924b51c2465a667f1c2bfda2be29818aa->leave($__internal_9e49bb7c7859c421919f425d3830852924b51c2465a667f1c2bfda2be29818aa_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_222ccb1c4f11fc1e86eed253632e5426ba71b87020b4e3c189bffde9cf95b384 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_222ccb1c4f11fc1e86eed253632e5426ba71b87020b4e3c189bffde9cf95b384->enter($__internal_222ccb1c4f11fc1e86eed253632e5426ba71b87020b4e3c189bffde9cf95b384_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@Twig/Exception/exception_full.html.twig"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_222ccb1c4f11fc1e86eed253632e5426ba71b87020b4e3c189bffde9cf95b384->leave($__internal_222ccb1c4f11fc1e86eed253632e5426ba71b87020b4e3c189bffde9cf95b384_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_4ff16c5a3411c7aa8bca88b4d0a76ccf46b3a6f75ac0d9159fe9606bad2f96d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ff16c5a3411c7aa8bca88b4d0a76ccf46b3a6f75ac0d9159fe9606bad2f96d0->enter($__internal_4ff16c5a3411c7aa8bca88b4d0a76ccf46b3a6f75ac0d9159fe9606bad2f96d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@Twig/Exception/exception_full.html.twig"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_4ff16c5a3411c7aa8bca88b4d0a76ccf46b3a6f75ac0d9159fe9606bad2f96d0->leave($__internal_4ff16c5a3411c7aa8bca88b4d0a76ccf46b3a6f75ac0d9159fe9606bad2f96d0_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_4adf402dfdc8e122c452dddc3c2f78be31b0fa1bd5fc288c3b71c71adedd0127 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4adf402dfdc8e122c452dddc3c2f78be31b0fa1bd5fc288c3b71c71adedd0127->enter($__internal_4adf402dfdc8e122c452dddc3c2f78be31b0fa1bd5fc288c3b71c71adedd0127_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@Twig/Exception/exception_full.html.twig"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_4adf402dfdc8e122c452dddc3c2f78be31b0fa1bd5fc288c3b71c71adedd0127->leave($__internal_4adf402dfdc8e122c452dddc3c2f78be31b0fa1bd5fc288c3b71c71adedd0127_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/var/www/html/Openbudget/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
