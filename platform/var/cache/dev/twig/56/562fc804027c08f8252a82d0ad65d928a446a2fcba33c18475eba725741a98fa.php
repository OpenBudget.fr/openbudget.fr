<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_074330e1f4fd39f7a71d40a3b01deb6a3943e9e2a93b175dca6baf302c84f270 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34762ddfbd5e7b8873bdba143111ef45b2403272ee443c5b68ac06d0f608664b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34762ddfbd5e7b8873bdba143111ef45b2403272ee443c5b68ac06d0f608664b->enter($__internal_34762ddfbd5e7b8873bdba143111ef45b2403272ee443c5b68ac06d0f608664b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_34762ddfbd5e7b8873bdba143111ef45b2403272ee443c5b68ac06d0f608664b->leave($__internal_34762ddfbd5e7b8873bdba143111ef45b2403272ee443c5b68ac06d0f608664b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_70e31336190aa3313217954dd2964d767e22b0f1db66b7d31046d5cfe2697e95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70e31336190aa3313217954dd2964d767e22b0f1db66b7d31046d5cfe2697e95->enter($__internal_70e31336190aa3313217954dd2964d767e22b0f1db66b7d31046d5cfe2697e95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@WebProfiler/Collector/router.html.twig"));

        
        $__internal_70e31336190aa3313217954dd2964d767e22b0f1db66b7d31046d5cfe2697e95->leave($__internal_70e31336190aa3313217954dd2964d767e22b0f1db66b7d31046d5cfe2697e95_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_c0760e4b00ac04ef3773586d1bb19b41824ced6d3b41312f52bc85e2c57fdeae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0760e4b00ac04ef3773586d1bb19b41824ced6d3b41312f52bc85e2c57fdeae->enter($__internal_c0760e4b00ac04ef3773586d1bb19b41824ced6d3b41312f52bc85e2c57fdeae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@WebProfiler/Collector/router.html.twig"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_c0760e4b00ac04ef3773586d1bb19b41824ced6d3b41312f52bc85e2c57fdeae->leave($__internal_c0760e4b00ac04ef3773586d1bb19b41824ced6d3b41312f52bc85e2c57fdeae_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_41095ccd3309b10273f35d33c370e65c80a64cf897a006d1cea7683dd4680bdc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41095ccd3309b10273f35d33c370e65c80a64cf897a006d1cea7683dd4680bdc->enter($__internal_41095ccd3309b10273f35d33c370e65c80a64cf897a006d1cea7683dd4680bdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@WebProfiler/Collector/router.html.twig"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_41095ccd3309b10273f35d33c370e65c80a64cf897a006d1cea7683dd4680bdc->leave($__internal_41095ccd3309b10273f35d33c370e65c80a64cf897a006d1cea7683dd4680bdc_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/html/Openbudget/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
