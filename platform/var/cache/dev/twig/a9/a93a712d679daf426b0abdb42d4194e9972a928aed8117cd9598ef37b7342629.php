<?php

/* base.html.twig */
class __TwigTemplate_c3320414d08f6182f265192ec1ee0e3d1c39d672a572dc025c54bc0a47c92874 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0dd7b809c0721d375781cd3e14d68645697c56aeff8cedd71699c20f6ede4f61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0dd7b809c0721d375781cd3e14d68645697c56aeff8cedd71699c20f6ede4f61->enter($__internal_0dd7b809c0721d375781cd3e14d68645697c56aeff8cedd71699c20f6ede4f61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_0dd7b809c0721d375781cd3e14d68645697c56aeff8cedd71699c20f6ede4f61->leave($__internal_0dd7b809c0721d375781cd3e14d68645697c56aeff8cedd71699c20f6ede4f61_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_ac90d77a0e7a9997b990238a805f6918801d2a2b060900d077aefb80559f1348 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac90d77a0e7a9997b990238a805f6918801d2a2b060900d077aefb80559f1348->enter($__internal_ac90d77a0e7a9997b990238a805f6918801d2a2b060900d077aefb80559f1348_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        echo "Welcome!";
        
        $__internal_ac90d77a0e7a9997b990238a805f6918801d2a2b060900d077aefb80559f1348->leave($__internal_ac90d77a0e7a9997b990238a805f6918801d2a2b060900d077aefb80559f1348_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8d36cd8b16727d4511191b3f9ac7713fb4302ea9d925005233835c4dcae5a713 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d36cd8b16727d4511191b3f9ac7713fb4302ea9d925005233835c4dcae5a713->enter($__internal_8d36cd8b16727d4511191b3f9ac7713fb4302ea9d925005233835c4dcae5a713_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        
        $__internal_8d36cd8b16727d4511191b3f9ac7713fb4302ea9d925005233835c4dcae5a713->leave($__internal_8d36cd8b16727d4511191b3f9ac7713fb4302ea9d925005233835c4dcae5a713_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_e78e1ea3efc51b94009cedcdc7a8ddc52bf4f3515ef29de1d6d408b968eee286 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e78e1ea3efc51b94009cedcdc7a8ddc52bf4f3515ef29de1d6d408b968eee286->enter($__internal_e78e1ea3efc51b94009cedcdc7a8ddc52bf4f3515ef29de1d6d408b968eee286_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        
        $__internal_e78e1ea3efc51b94009cedcdc7a8ddc52bf4f3515ef29de1d6d408b968eee286->leave($__internal_e78e1ea3efc51b94009cedcdc7a8ddc52bf4f3515ef29de1d6d408b968eee286_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_0e04068bc41751248a9ce553c82ebf90baa64e59f94423feb143ce2461bf0dbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e04068bc41751248a9ce553c82ebf90baa64e59f94423feb143ce2461bf0dbe->enter($__internal_0e04068bc41751248a9ce553c82ebf90baa64e59f94423feb143ce2461bf0dbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        
        $__internal_0e04068bc41751248a9ce553c82ebf90baa64e59f94423feb143ce2461bf0dbe->leave($__internal_0e04068bc41751248a9ce553c82ebf90baa64e59f94423feb143ce2461bf0dbe_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/html/openbudget.fr/platform/app/Resources/views/base.html.twig");
    }
}
