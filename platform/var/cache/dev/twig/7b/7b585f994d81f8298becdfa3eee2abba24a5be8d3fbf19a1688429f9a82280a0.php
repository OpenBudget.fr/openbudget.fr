<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_eb0dd0abb6f570164c0b08e2a468f2abd6e5c7d4d08b779bc26352a9a2ac4bda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04f660977b6de927a714d2775be96dca7455bb93be87119a9af18768fb9574b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04f660977b6de927a714d2775be96dca7455bb93be87119a9af18768fb9574b3->enter($__internal_04f660977b6de927a714d2775be96dca7455bb93be87119a9af18768fb9574b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_04f660977b6de927a714d2775be96dca7455bb93be87119a9af18768fb9574b3->leave($__internal_04f660977b6de927a714d2775be96dca7455bb93be87119a9af18768fb9574b3_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c2b2885db48056aaa0103ae5d5b541117bd67fa4f1625804b9211c62507e542c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2b2885db48056aaa0103ae5d5b541117bd67fa4f1625804b9211c62507e542c->enter($__internal_c2b2885db48056aaa0103ae5d5b541117bd67fa4f1625804b9211c62507e542c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@WebProfiler/Collector/router.html.twig"));

        
        $__internal_c2b2885db48056aaa0103ae5d5b541117bd67fa4f1625804b9211c62507e542c->leave($__internal_c2b2885db48056aaa0103ae5d5b541117bd67fa4f1625804b9211c62507e542c_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f5a2037fdf435b1710e2fde0d4e3545e6e1684b06b8f96c0cd8273dca8f35ee1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5a2037fdf435b1710e2fde0d4e3545e6e1684b06b8f96c0cd8273dca8f35ee1->enter($__internal_f5a2037fdf435b1710e2fde0d4e3545e6e1684b06b8f96c0cd8273dca8f35ee1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@WebProfiler/Collector/router.html.twig"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_f5a2037fdf435b1710e2fde0d4e3545e6e1684b06b8f96c0cd8273dca8f35ee1->leave($__internal_f5a2037fdf435b1710e2fde0d4e3545e6e1684b06b8f96c0cd8273dca8f35ee1_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_09375937599535d5e0eb7c99c6d60efb23a1f5b3e59e5662ba3757b70e3988b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09375937599535d5e0eb7c99c6d60efb23a1f5b3e59e5662ba3757b70e3988b5->enter($__internal_09375937599535d5e0eb7c99c6d60efb23a1f5b3e59e5662ba3757b70e3988b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "@WebProfiler/Collector/router.html.twig"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_09375937599535d5e0eb7c99c6d60efb23a1f5b3e59e5662ba3757b70e3988b5->leave($__internal_09375937599535d5e0eb7c99c6d60efb23a1f5b3e59e5662ba3757b70e3988b5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/html/openbudget.fr/platform/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
