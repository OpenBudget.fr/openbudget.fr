<?php

/* base.html.twig */
class __TwigTemplate_43996757d1efd91f2f8e78dd049dc74c5bdc7aa31fce1e67ab5c04f5a29c7a03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebca2a75b668fb9c2f552d92847aff0063df7e09400f3cf1e517b3ab1bbb2c2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebca2a75b668fb9c2f552d92847aff0063df7e09400f3cf1e517b3ab1bbb2c2a->enter($__internal_ebca2a75b668fb9c2f552d92847aff0063df7e09400f3cf1e517b3ab1bbb2c2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_ebca2a75b668fb9c2f552d92847aff0063df7e09400f3cf1e517b3ab1bbb2c2a->leave($__internal_ebca2a75b668fb9c2f552d92847aff0063df7e09400f3cf1e517b3ab1bbb2c2a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_12337bc17b8fd8a34809c526acf500877f36324b27909a67dd8b05fbb92b0e3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12337bc17b8fd8a34809c526acf500877f36324b27909a67dd8b05fbb92b0e3d->enter($__internal_12337bc17b8fd8a34809c526acf500877f36324b27909a67dd8b05fbb92b0e3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        echo "Welcome!";
        
        $__internal_12337bc17b8fd8a34809c526acf500877f36324b27909a67dd8b05fbb92b0e3d->leave($__internal_12337bc17b8fd8a34809c526acf500877f36324b27909a67dd8b05fbb92b0e3d_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_bf4868d947bccb89c52e49b39acb54107a6c92e9cc1cc3a7c09bc39d9e7c5565 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf4868d947bccb89c52e49b39acb54107a6c92e9cc1cc3a7c09bc39d9e7c5565->enter($__internal_bf4868d947bccb89c52e49b39acb54107a6c92e9cc1cc3a7c09bc39d9e7c5565_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        
        $__internal_bf4868d947bccb89c52e49b39acb54107a6c92e9cc1cc3a7c09bc39d9e7c5565->leave($__internal_bf4868d947bccb89c52e49b39acb54107a6c92e9cc1cc3a7c09bc39d9e7c5565_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_563d6cac72811ee5e52425748809ce710644132944486eb8828c02df4134d199 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_563d6cac72811ee5e52425748809ce710644132944486eb8828c02df4134d199->enter($__internal_563d6cac72811ee5e52425748809ce710644132944486eb8828c02df4134d199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        
        $__internal_563d6cac72811ee5e52425748809ce710644132944486eb8828c02df4134d199->leave($__internal_563d6cac72811ee5e52425748809ce710644132944486eb8828c02df4134d199_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9b7bd170fe12ff415aa8150936337f6ee024dc73f92bebe34e97c9afe07e3722 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b7bd170fe12ff415aa8150936337f6ee024dc73f92bebe34e97c9afe07e3722->enter($__internal_9b7bd170fe12ff415aa8150936337f6ee024dc73f92bebe34e97c9afe07e3722_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "base.html.twig"));

        
        $__internal_9b7bd170fe12ff415aa8150936337f6ee024dc73f92bebe34e97c9afe07e3722->leave($__internal_9b7bd170fe12ff415aa8150936337f6ee024dc73f92bebe34e97c9afe07e3722_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/html/Openbudget/app/Resources/views/base.html.twig");
    }
}
